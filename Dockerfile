# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang

# Copy the local package files to the container's workspace.
ADD . /go/src/itcalc

RUN cd /go/src/itcalc && go build -o itcalc main.go && chmod 755 itcalc

WORKDIR /go/src/itcalc
# Run the outyet command by default when the container starts.
ENTRYPOINT /go/src/itcalc/itcalc

# Document that the service listens on port 8080.
EXPOSE 8080