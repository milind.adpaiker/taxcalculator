/*
 * Income Tax calculator
 *
 * This is a service to calculate income tax payable by an Indian resident or an Indian company
 *
 * API version: 1.0.0
 * Contact: ma@swagger.io
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

import (
	"reflect"
	"testing"
)

func Test_calculateTax(t *testing.T) {
	type args struct {
		inc Income
	}
	tests := []struct {
		name string
		args args
		want TaxDue
	}{
		{
			name: "tax-receivable",
			args: args{inc: Income{Income: 200, Exemption: 300}},
			want: TaxDue{Income: 200, Exemption: 300, Receivable: 100, Payable: 0},
		},
		{
			name: "tax-no-dues",
			args: args{inc: Income{Income: 100000, Exemption: 3490}},
			want: TaxDue{Income: 100000, Exemption: 3490, Receivable: 0, Payable: 0},
		},
		{
			name: "tax-payable-1st bracket",
			args: args{inc: Income{Income: 300000, Exemption: 0}},
			want: TaxDue{Income: 300000, Exemption: 0, Receivable: 0, Payable: 2500},
		},
		{
			name: "tax-payable-last-bracket",
			args: args{inc: Income{Income: 2000000, Exemption: 0}},
			want: TaxDue{Income: 2000000, Exemption: 0, Receivable: 0, Payable: 412500},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculateTax(tt.args.inc); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("calculateTax() = %+v, want %+v", got, tt.want)
			}
		})
	}
}
