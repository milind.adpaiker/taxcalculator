/*
 * Income Tax calculator
 *
 * This is a service to calculate income tax payable by an Indian resident or an Indian company
 *
 * API version: 1.0.0
 * Contact: ma@swagger.io
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

import (
	"encoding/json"
	"log"
	"net/http"
)

func CalculateTaxPost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	var inc Income
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&inc)
	if err != nil {
		log.Println("Error: ", err)
	}

	td := calculateTax(inc)
	json.NewEncoder(w).Encode(td)
}

func calculateTax(inc Income) TaxDue {
	taxable := inc.Income - inc.Exemption
	td := TaxDue{Income: inc.Income, Exemption: inc.Exemption}

	if taxable <= 0 {
		td.Payable = 0
		td.Receivable = -taxable
	} else if taxable > 0 && taxable <= 250000 {
		td.Payable = 0
	} else if taxable > 250000 && taxable <= 500000 {
		td.Payable = (taxable - 250000) * 5 / 100
	} else if taxable > 500000 && taxable <= 1000000 {
		td.Payable = 0 + 12500 + (taxable-500000)*20/100
	} else if taxable > 1000000 {
		td.Payable = 0 + 12500 + 100000 + (taxable-1000000)*30/100
	}
	return td
}
